#pragma once

#include "IList.h"

#include "WaypointList.h"
#include "ObstacleList.h"

#include <CL/cl.hpp>

#include <vector>


class AgentList : public IList
{
public:
    static const void* getPositionOffset();
    static const void* getVelocityOffset();
    static const void* getRadiusOffset();
    static const void* getIsActiveOffset();

    static int getStride() { return sizeof(agent_t); }

    void add(cl_float2 position);

    void compute(const WaypointList& waypoints, const ObstacleList& obstacles);
    void move(float deltaTime);

    int getSize() const { return (int)m_list.size(); }
    const void* getBuffer() const { return m_list.data(); }
    int getBufferSize() const { return (int)(m_list.size() * sizeof(agent_t)); }

private:
    struct agent_t {
        cl_float radius = 0.2f;
        cl_float max_speed;

        cl_float2 position;
        cl_float2 velocity = {{ 0.0f, 0.0f }};
        cl_float2 acceleration = {{ 0.0f, 0.0f }};

        cl_int waypoint = 0;
        cl_bool isActive = CL_TRUE;
    };

    cl_float2 waypoint_get_direction(const WaypointList::waypoint_t* waypoint, const cl_float2& origin, bool* reached);

    cl_float2 obstacle_get_closest_point(const ObstacleList::obstacle_t* obstacle, const cl_float2& position);
    cl_float2 get_obstacle_force(const agent_t* agent, const std::vector<ObstacleList::obstacle_t>& obstacles);

    cl_float2 get_look_ahead_force(const agent_t* agent, const cl_float2& direction);
    cl_float2 get_social_force(const agent_t* agent);

    std::vector<agent_t> m_list;
};
