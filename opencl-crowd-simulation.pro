CONFIG += c++14

QT += widgets

TARGET = opencl-crowd-simulation
TEMPLATE = app


# Windows (Visual Studio)
win32 {
    INCLUDEPATH += $$(CUDA_PATH)/include $$(GLEW_PATH)/include $$(GLM_PATH)/include
    LIBS += -L$$(CUDA_PATH)/lib/x64 -L$$(GLEW_PATH)/lib
    LIBS += -lOpenCL -lOpenGL32 -lglew32
}
# Mac OSX (clang)
else:macx {
    INCLUDEPATH += /usr/local/include
    LIBS += -L/usr/local/lib
    LIBS += -framework OpenCL -framework OpenGL
    LIBS += -lGLEW
}
# Unix (gcc and clang)
else {
    CONFIG -= c++14
    QMAKE_CXXFLAGS += -std=gnu++14
    LIBS += -lOpenCL -lGLEW
}


SOURCES += main.cpp \
    AgentRenderer.cpp \
    ObstacleRenderer.cpp \
    WaypointRenderer.cpp \
    SimulationWindow.cpp \
    ShaderProgram.cpp \
    Shader.cpp \
    Simulation.cpp \
    AgentList.cpp \
    WaypointList.cpp \
    ObstacleList.cpp

HEADERS  += \
    AgentRenderer.h \
    ObstacleRenderer.h \
    WaypointRenderer.h \
    SimulationWindow.h \
    AbstractRenderer.h \
    ShaderProgram.h \
    Shader.h \
    Simulation.h \
    IList.h \
    AgentList.h \
    WaypointList.h \
    ObstacleList.h

DISTFILES += \
    agent_compute_kernel.cl \
    agent_move_kernel.cl \
    agent.cl \
    waypoint.cl \
    obstacle.cl \
    agent.vert \
    agent.geom \
    waypoint.vert \
    waypoint.geom \
    obstacle.vert \
    white.frag \
    utils.cl \
    obstacle.geom
