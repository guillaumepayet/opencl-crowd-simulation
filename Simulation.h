#pragma once

#define __CL_ENABLE_EXCEPTIONS

#include "AbstractRenderer.h"

#include "AgentList.h"
#include "WaypointList.h"
#include "ObstacleList.h"


#define SCENE_WIDTH		80.0f
#define SCENE_HEIGHT	60.0f


class Simulation
{
public:
    Simulation(size_t agentCount = 10000, size_t workgroupSize = 125);
    ~Simulation();

    void registerAgentRenderer(AbstractRenderer* renderer);
    void registerWaypointRenderer(AbstractRenderer* renderer);
    void registerObstacleRenderer(AbstractRenderer* renderer);

    void initialize();
    void animate(float deltaTime);

private:
    void initContext();

    static cl::Kernel* loadKernel(const cl::Context& context, const char* filename, const char* name);

    cl::NDRange m_global, m_local;

    AgentList m_agents;
    WaypointList m_waypoints;
    ObstacleList m_obstacles;

    AbstractRenderer* m_agentRenderer;
    AbstractRenderer* m_waypointRenderer;
    AbstractRenderer* m_obstacleRenderer;

    bool m_hasSharingExt;
    cl::Context* m_context;
    cl::CommandQueue* m_queue;

    std::vector<cl::Memory> m_buffers;

    cl::Kernel* m_computeKernel;
    cl::Kernel* m_moveKernel;
};
