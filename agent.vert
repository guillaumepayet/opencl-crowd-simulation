#version 330

layout (location = 0) in vec4 positionIn;
layout (location = 1) in vec2 velocityIn;
layout (location = 2) in float radiusIn;
layout (location = 3) in int isActiveIn;

out vec2 velocity;
out float radius;
out int isActive;

void main()
{
    gl_Position = positionIn;
    velocity = velocityIn;
    radius = radiusIn;
    isActive = isActiveIn;
}
