#include "Simulation.h"

#include "AgentList.h"
#include "WaypointList.h"
#include "ObstacleList.h"

#if defined(__APPLE__)
#   include <CGLCurrent.h>
#elif defined(_WIN32)
#   include <Windows.h>
#else
#   include <GL/glx.h>
#endif

#include <iostream>
#include <fstream>
#include <random>


#define TIME_RATIO		1.0f


Simulation::Simulation(size_t agentCount, size_t workgroupSize)
    : m_global(agentCount)
    , m_local(workgroupSize)
    , m_hasSharingExt(false)
{
    std::uniform_real_distribution<float> xDist(-SCENE_WIDTH / 2.0f, SCENE_WIDTH / 2.0f - 15.0f);
    std::uniform_real_distribution<float> yDist(-SCENE_HEIGHT / 2.0f, SCENE_HEIGHT / 2.0f);

    std::default_random_engine generator;


    for (size_t i = 0; i < agentCount; i++) {
        m_agents.add({{ xDist(generator), yDist(generator) }});
    }

    m_waypoints.add({{  SCENE_WIDTH / 2.0f, 0.0f }}, 10.0f, CL_TRUE);

    m_obstacles.add({{ -SCENE_WIDTH / 2.0f,  SCENE_HEIGHT / 2.0f }}, {{ -SCENE_WIDTH / 2.0f, -SCENE_HEIGHT / 2.0f }});
    m_obstacles.add({{ -SCENE_WIDTH / 2.0f, -SCENE_HEIGHT / 2.0f }}, {{  SCENE_WIDTH / 2.0f, -SCENE_HEIGHT / 2.0f }});
    m_obstacles.add({{  SCENE_WIDTH / 2.0f, -SCENE_HEIGHT / 2.0f }}, {{  SCENE_WIDTH / 2.0f,  SCENE_HEIGHT / 2.0f }});
    m_obstacles.add({{  SCENE_WIDTH / 2.0f,  SCENE_HEIGHT / 2.0f }}, {{ -SCENE_WIDTH / 2.0f,  SCENE_HEIGHT / 2.0f }});

    m_obstacles.add({{ SCENE_WIDTH / 2.0f - 15.0f, SCENE_HEIGHT / 2.0f }},
                    {{ SCENE_WIDTH / 2.0f - 15.0f, 1.0f }});

    m_obstacles.add({{ SCENE_WIDTH / 2.0f - 15.0f, -1.0f }},
                    {{ SCENE_WIDTH / 2.0f - 15.0f, -SCENE_HEIGHT / 2.0f }});
}

Simulation::~Simulation()
{
    delete m_moveKernel;
    delete m_computeKernel;
    m_buffers.clear();
    delete m_queue;
    delete m_context;
}


void Simulation::registerAgentRenderer(AbstractRenderer *renderer)
{
    m_agentRenderer = renderer;
    m_agentRenderer->setList(&m_agents);
}

void Simulation::registerWaypointRenderer(AbstractRenderer *renderer)
{
    m_waypointRenderer = renderer;
    m_waypointRenderer->setList(&m_waypoints);
}

void Simulation::registerObstacleRenderer(AbstractRenderer *renderer)
{
    m_obstacleRenderer = renderer;
    m_obstacleRenderer->setList(&m_obstacles);
}


void Simulation::initialize()
{
    try {
        initContext();

        m_queue = new cl::CommandQueue(*m_context);


        if (m_hasSharingExt) {
            m_buffers.push_back(cl::BufferGL(*m_context, CL_MEM_READ_WRITE, m_agentRenderer->getVBO()));
            m_buffers.push_back(cl::BufferGL(*m_context, CL_MEM_READ_ONLY, m_waypointRenderer->getVBO()));
            m_buffers.push_back(cl::BufferGL(*m_context, CL_MEM_READ_ONLY, m_obstacleRenderer->getVBO()));
        }
        else {
            m_buffers.push_back(cl::Buffer(*m_context, CL_MEM_READ_WRITE, m_agents.getBufferSize()));
            m_buffers.push_back(cl::Buffer(*m_context, CL_MEM_READ_ONLY, m_waypoints.getBufferSize()));
            m_buffers.push_back(cl::Buffer(*m_context, CL_MEM_READ_ONLY, m_obstacles.getBufferSize()));

            m_queue->enqueueWriteBuffer(reinterpret_cast<cl::Buffer&>(m_buffers[0]), CL_FALSE, 0, m_agents.getBufferSize(), m_agents.getBuffer());
            m_queue->enqueueWriteBuffer(reinterpret_cast<cl::Buffer&>(m_buffers[1]), CL_FALSE, 0, m_waypoints.getBufferSize(), m_waypoints.getBuffer());
            m_queue->enqueueWriteBuffer(reinterpret_cast<cl::Buffer&>(m_buffers[2]), CL_FALSE, 0, m_obstacles.getBufferSize(), m_obstacles.getBuffer());
        }


        m_computeKernel = loadKernel(*m_context, "agent_compute_kernel.cl", "compute");

        m_computeKernel->setArg(0, m_agents.getSize());
        m_computeKernel->setArg(1, m_buffers[0]);
        m_computeKernel->setArg(2, m_waypoints.getSize());
        m_computeKernel->setArg(3, m_buffers[1]);
        m_computeKernel->setArg(4, m_obstacles.getSize());
        m_computeKernel->setArg(5, m_buffers[2]);


        m_moveKernel = loadKernel(*m_context, "agent_move_kernel.cl", "move");

        m_moveKernel->setArg(1, m_buffers[0]);
    }
    catch (cl::Error error) {
        std::cerr << error.what() << " (" << error.err() << ")" << std::endl;
        exit(2);
    }
    catch (...) {
        std::cerr << "Unknown error." << std::endl;
        exit(-1);
    }
}

void Simulation::animate(float deltaTime)
{
//    m_agents.compute(m_waypoints, m_obstacles);
//    m_agents.move(deltaTime);
//    m_agentRenderer->setList(&m_agents);

    try {
        m_moveKernel->setArg(0, TIME_RATIO * deltaTime);

        if (m_hasSharingExt) {
            m_queue->enqueueAcquireGLObjects(&m_buffers);
        }

        m_queue->enqueueNDRangeKernel(*m_computeKernel, cl::NullRange, m_global, m_local);
        m_queue->enqueueNDRangeKernel(*m_moveKernel, cl::NullRange, m_global, m_local);

        if (m_hasSharingExt) {
            m_queue->enqueueReleaseGLObjects(&m_buffers);
        }
        else {
            m_queue->enqueueReadBuffer(reinterpret_cast<cl::Buffer&>(m_buffers[0]), CL_TRUE, 0, m_agents.getBufferSize(), const_cast<void*>(m_agents.getBuffer()));
            m_agentRenderer->setList(&m_agents);
        }
    }
    catch (cl::Error error) {
        std::cerr << error.what() << " (" << error.err() << ")" << std::endl;
        exit(2);
    }
    catch (...) {
        std::cerr << "Unknown error." << std::endl;
        exit(-1);
    }
}


void Simulation::initContext()
{
    std::vector<cl_context_properties> properties;

#if defined(__APPLE__)

    CGLContextObj kCGLContext = CGLGetCurrentContext();
    CGLShareGroupObj kCGLShareGroup = CGLGetShareGroup(kCGLContext);

    properties.push_back(CL_CONTEXT_PROPERTY_USE_CGL_SHAREGROUP_APPLE);
    properties.push_back(reinterpret_cast<cl_context_properties>(kCGLShareGroup));

#else

    std::vector<cl::Platform> platforms;
    cl::Platform::get(&platforms);

    size_t selectedPlatform = 0;

    if (platforms.size() > 1) {
//        std::cout << platforms.size() << " OpenCL platform(s) available:" << std::endl;

        for (size_t i = 0; i < platforms.size(); i++) {
            std::string version, vendor;
            platforms[i].getInfo(CL_PLATFORM_VERSION, &version);
            platforms[i].getInfo(CL_PLATFORM_VENDOR, &vendor);
//            std::cout << "  " << version.c_str() << " from " << vendor.c_str() << std::endl;

            if (vendor.find("NVIDIA") != std::string::npos) {
                selectedPlatform = i;
                break;
            }
        }
    }

    cl::Platform& platform = platforms[selectedPlatform];
    properties.push_back(CL_CONTEXT_PLATFORM);
    properties.push_back(reinterpret_cast<cl_context_properties>(platform()));

    std::vector<cl::Device> devices;
    platform.getDevices(CL_DEVICE_TYPE_GPU, &devices);

    for (const cl::Device& device : devices) {
        std::string extensions;
        device.getInfo(CL_DEVICE_EXTENSIONS, &extensions);

        if (extensions.find("cl_khr_gl_sharing") != std::string::npos) {
            m_hasSharingExt = true;
            break;
        }
    }

    if (!m_hasSharingExt) {
//        std::cout << "OpenGL sharing not available." << std::endl;
    }
    else {
//        std::cout << "OpenGL sharing available." << std::endl;

        properties.push_back(CL_GL_CONTEXT_KHR);

#   if defined(_WIN32)

        properties.push_back(reinterpret_cast<cl_context_properties>(wglGetCurrentContext()));
        properties.push_back(CL_WGL_HDC_KHR);
        properties.push_back(reinterpret_cast<cl_context_properties>(wglGetCurrentDC()));

#   else

        properties.push_back(reinterpret_cast<cl_context_properties>(glXGetCurrentContext()));
        properties.push_back(CL_GLX_DISPLAY_KHR);
        properties.push_back(reinterpret_cast<cl_context_properties>(glXGetCurrentDisplay()));

#   endif

    }

#endif

    properties.push_back(0);
    m_context = new cl::Context(CL_DEVICE_TYPE_GPU, properties.data());
}

cl::Kernel* Simulation::loadKernel(const cl::Context& context, const char* filename, const char* name)
{
    std::ifstream file(filename);

    std::string source((std::istreambuf_iterator<char>(file)),
                       (std::istreambuf_iterator<char>()));

    cl::Program::Sources sources;
    sources.push_back(std::make_pair(source.c_str(), source.length() + 1));

    cl::Program program(context, sources);
    program.build();

    return new cl::Kernel(program, name);
}
