#pragma once

#include "IList.h"

#include <glm/glm.hpp>

#ifdef _WIN32
    #include <Windows.h>
#endif

#ifdef __APPLE__
#   include <OpenGL/gl.h>
#else
#   include <GL/gl.h>
#endif


class AbstractRenderer
{
public:
    virtual ~AbstractRenderer() {}

    virtual void setList(const IList* list) = 0;
    virtual void setMVPMatrix(const glm::mat4& matrix) = 0;
    virtual void render() = 0;

    GLuint getVBO() const { return m_vbo; }

protected:
    GLuint m_vbo;
};
