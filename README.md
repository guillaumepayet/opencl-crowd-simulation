# OpenCL Crowd Simulation #

This is a crowd simulation written in OpenCL. It is largely based on [PedSim](http://pedsim.silmaril.org/) but a few features were removed (not easy to port to GPGPU) like, for example, the space partitioning.

The simulation is run in a window as a standalone with basic graphics but could easily be injected into another program with another more complex (OpenGL shader-based) renderer.

## Control Flow ##

The simulation is run in a Qt window which handles the OpenGL initialisation (the SimulationWindow class).

There are 3 types of objects, the agents, the waypoints and the obstacles. Each of them have their own renderers and these are the ones initialised right after the window is initialised.

Once the renderers initialised, the simulation is run one step per frame. The Simulation class handles the creation, management and execution of the OpenCL kernels. The whole simulation is described in the .cl files.

## Simulation ##

The simulation is based on the social forces model. Each agent is handled in its own OpenCL thread. A kernel is first executed which computes the resulting force on each agent. It combines the repelling forces from the other agents and the obstacles with the attraction force from the goal. A second kernel is executed to actually move each agent.

More information can be found in my honours report [here](https://www.cosc.canterbury.ac.nz/research/reports/HonsReps/2016/hons_1601.pdf) (2191 kB).

## How to Build ##

In order to compile and run this program, you will need:

* OpenCL 1.2
* OpenGL 3.3
* Qt 5.5 (with OpenGL support and QtCreator)
* It is compatible with Windows, Linux and Mac OSX

Once you have everything, just open the opencl-crowd-simulation.pro file in QtCreator and build!

Make sure the OpenCL kernels and the OpenGL shaders are in the working directory before running. You can also edit them without having to re-compile the application.

