#include "agent.cl"
#include "waypoint.cl"
#include "obstacle.cl"

#include "utils.cl"


#define OBSTACLE_FORCE_SIGMA    0.8f

#define NEIGHBORHOOD_RADIUS 20.0f
#define SOCIAL_RADIUS       2.0f

#define PI  3.1415927f

#define LAMBDA_IMPORTANCE   2.0f
#define GAMMA               0.35f
#define N                   2.0f
#define N_PRIME             3.0f

#define DESIRED_FORCE_FACTOR    1.0f
#define OBSTACLE_FORCE_FACTOR   10.0f
#define LOOK_AHEAD_FORCE_FACTOR 1.0f
#define SOCIAL_FORCE_FACTOR     2.1f


float2 get_obstacle_force(const __global agent_t* agent, int num_obstacles, __constant obstacle_t* obstacles)
{
    float2 min_diff;
    float min_distance = 1e9f;

    for (int i = 0; i < num_obstacles; i++) {
        __constant obstacle_t* obstacle = &obstacles[i];

        float2 closest_point = obstacle_get_closest_point(obstacle, agent->position);
        float2 diff = agent->position - closest_point;
        float distance = LENGTH(diff);

        if (distance < min_distance) {
            min_distance = distance;
            min_diff = diff;
        }
    }

    float distance = min_distance - agent->radius;
    float force_amount = native_exp(-distance / OBSTACLE_FORCE_SIGMA);
    return force_amount * min_diff / min_distance;
}


float2 get_look_ahead_force(const __global agent_t* agent, float2 direction, int num_agents, const __global agent_t* agents)
{
    int count = 0;

    for (int i = 0; i < num_agents; i++) {
        const __global agent_t* other = &agents[i];

        if (!other->isActive || other == agent) continue;


        float2 diff = other->position - agent->position;

        if (SQUARED_LENGTH(diff) > SQUARED(NEIGHBORHOOD_RADIUS)) continue;


        float direction_angle = atan2(direction.y, direction.x);
        float vv = direction_angle - atan2(other->velocity.y, other->velocity.x);

        if (vv >  PI) vv -= 2.0f * PI;
        if (vv < -PI) vv += 2.0f * PI;

        if (fabs(vv) > 2.5f) {
            float s = atan2(diff.y, diff.x) - direction_angle;

            if (s >  PI) s -= 2.0f * PI;
            if (s < -PI) s += 2.0f * PI;

            if ((-0.3f < s) && (s < 0.0f)) {
                count--;
            }
            else if ((0.0f < s) && (s < 0.3f)) {
                count++;
            }
        }
    }

    float2 force;

    if (count < 0) {
        force.x =  0.5f * direction.y;
        force.y = -0.5f * direction.x;
    }
    else {
        force.x = -0.5f * direction.y;
        force.y =  0.5f * direction.x;
    }

    return force;
}

float2 get_social_force(const __global agent_t* agent, int num_agents, const __global agent_t* agents)
{
    float2 force = 0.0f;

    for (int i = 0; i < num_agents; i++) {
        const __global agent_t* other = &agents[i];

        if (!other->isActive || other == agent) continue;


        float2 diff = other->position - agent->position;
        float squared_distance = SQUARED_LENGTH(diff);

        if (squared_distance > SQUARED(SOCIAL_RADIUS)) continue;


        float distance = native_sqrt(squared_distance);
        float2 diff_direction = diff / distance;

        float2 diff_velocity = agent->velocity - other->velocity;

        float2 interaction = LAMBDA_IMPORTANCE * diff_velocity + diff_direction;
        float interaction_length = LENGTH(interaction);
        float2 interaction_direction = interaction / interaction_length;

        float theta = atan2(diff_direction.y, diff_direction.x) - atan2(interaction_direction.y, interaction_direction.x);

        if (theta >  PI) theta -= 2.0f * PI;
        if (theta < -PI) theta += 2.0f * PI;

        float theta_sign = (theta < 0.0f) ? -1.0f : 1.0f;

        float B = GAMMA * interaction_length;
        float force_velocity_amount = -native_exp(-distance / B - SQUARED(N_PRIME * B * theta));
        float force_angle_amount = -theta_sign * native_exp(-distance / B - SQUARED(N * B * theta));

        float2 force_velocity = force_velocity_amount * interaction_direction;
        float2 force_angle = force_angle_amount * (float2)(-interaction_direction.y, interaction_direction.x);

        force += force_velocity + force_angle;
    }

    return force;
}


__kernel void compute(
        int num_agents, __global agent_t* agents,
        int num_waypoints, __constant waypoint_t* waypoints,
        int num_obstacles, __constant obstacle_t* obstacles)
{
    size_t i = get_global_id(0);
    __global agent_t* agent = &agents[i];

    if (!agent->isActive) return;


    __constant waypoint_t* waypoint = &waypoints[agent->waypoint];
    bool reached = false;

    float2 waypoint_direction = waypoint_get_direction(waypoint, agent->position, &reached);

    if (reached) {
        if (waypoint->isFinal) {
            agent->isActive = false;
        }
        else {
            agent->waypoint = (agent->waypoint + 1) % num_waypoints;
        }
    }

    float2 desired_force = agent->max_speed * waypoint_direction;
    float2 obstacle_force = get_obstacle_force(agent, num_obstacles, obstacles);

    float2 look_ahead_force = get_look_ahead_force(agent, waypoint_direction, num_agents, agents);
    float2 social_force = get_social_force(agent, num_agents, agents);

    agent->acceleration = (float2)(0.0f, 0.0f)
            + DESIRED_FORCE_FACTOR * desired_force
            + OBSTACLE_FORCE_FACTOR * obstacle_force
            + LOOK_AHEAD_FORCE_FACTOR * look_ahead_force
            + SOCIAL_FORCE_FACTOR * social_force
            + (float2)(0.0f, 0.0f);
}
