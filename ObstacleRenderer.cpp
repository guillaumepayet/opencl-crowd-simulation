#include <GL/glew.h>

#include "ObstacleRenderer.h"

#include "ObstacleList.h"


ObstacleRenderer::ObstacleRenderer()
{
    m_program.attachShader(Shader(Shader::Vertex, "obstacle.vert"));
    m_program.attachShader(Shader(Shader::Geometry, "obstacle.geom"));
    m_program.attachShader(Shader(Shader::Fragment, "white.frag"));
    m_program.link();

    m_mvpMatrixLoc = m_program.getUniformLocation("mvpMatrix");


    glGenVertexArrays(1, &m_vao);
    glBindVertexArray(m_vao);

    glGenBuffers(1, &m_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, ObstacleList::getStride(), ObstacleList::getAOffset());
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, ObstacleList::getStride(), ObstacleList::getBOffset());
}

ObstacleRenderer::~ObstacleRenderer()
{
    glDeleteVertexArrays(1, &m_vao);
    glDeleteBuffers(1, &m_vbo);
}


void ObstacleRenderer::setList(const IList* list)
{
    m_list = list;
    glBindVertexArray(m_vao);
    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glBufferData(GL_ARRAY_BUFFER, m_list->getBufferSize(), m_list->getBuffer(), GL_STATIC_DRAW);
}

void ObstacleRenderer::setMVPMatrix(const glm::mat4& matrix)
{
    m_program.bind();
    m_program.setUniformMatrix(m_mvpMatrixLoc, matrix);
}


void ObstacleRenderer::render()
{
    m_program.bind();
    glBindVertexArray(m_vao);
    glDrawArrays(GL_POINTS, 0, m_list->getSize());
}
