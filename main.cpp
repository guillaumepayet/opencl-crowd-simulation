#include "SimulationWindow.h"

#include <QApplication>

#include <iostream>


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    SimulationWindow window;

    window.show();

    return app.exec();
}
