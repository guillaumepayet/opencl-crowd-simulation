#include <GL/glew.h>

#include "Shader.h"

#include <iostream>
#include <fstream>


Shader::Shader(Shader::Type type, const char* filename)
    : m_shader(glCreateShader(type))
{
    std::ifstream file(filename);

    if (!file.is_open()) {
        std::cerr << "Could not load shader: " << filename << std::endl;
    }

    std::string buffer((std::istreambuf_iterator<char>(file)),
                       (std::istreambuf_iterator<char>()));

    const char* source = buffer.c_str();
    glShaderSource(m_shader, 1, &source, nullptr);
    glCompileShader(m_shader);

    GLint status;
    glGetShaderiv(m_shader, GL_COMPILE_STATUS, &status);

    if (status == GL_FALSE) {
        GLsizei infoLogLength;
        glGetShaderiv(m_shader, GL_INFO_LOG_LENGTH, &infoLogLength);

        GLchar* infoLog = new GLchar[infoLogLength + 1];
        glGetShaderInfoLog(m_shader, infoLogLength, nullptr, infoLog);

        std::cerr << "Could not compile shader: " << infoLog << std::endl;

        delete[] infoLog;
    }
}

Shader::~Shader()
{
    glDeleteShader(m_shader);
}
