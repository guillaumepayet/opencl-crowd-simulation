#include "agent.cl"

#include "utils.cl"


__kernel void move(float deltaTime, __global agent_t* agents)
{
    size_t i = get_global_id(0);
    __global agent_t* agent = &agents[i];

    if (agent->isActive) {
        agent->velocity += deltaTime * agent->acceleration;

        float speed = LENGTH(agent->velocity);

        if (speed > agent->max_speed) {
            agent->velocity *= (agent->max_speed / speed);
        }

        agent->position += deltaTime * agent->velocity;
    }
}
