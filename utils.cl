#pragma once

#define SQUARED(x)  (x * x)

#define SQUARED_LENGTH(v)   (SQUARED(v.x) + SQUARED(v.y))

#define LENGTH(v)   native_sqrt(SQUARED_LENGTH(v))
