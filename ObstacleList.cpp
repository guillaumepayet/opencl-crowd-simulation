#include "ObstacleList.h"


void ObstacleList::add(cl_float2 a, cl_float2 b)
{
    m_list.push_back({ a, b });
}


const void* ObstacleList::getAOffset()
{
    return (const void*)offsetof(obstacle_t, a);
}

const void* ObstacleList::getBOffset()
{
    return (const void*)offsetof(obstacle_t, b);
}
