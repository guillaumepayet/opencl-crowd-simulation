#include <GL/glew.h>

#include "SimulationWindow.h"

#include "AgentRenderer.h"
#include "WaypointRenderer.h"
#include "ObstacleRenderer.h"

#include <glm/gtc/matrix_transform.hpp>

#include <iostream>
#include <sstream>


#define REPORT_DELAY 1.0f


SimulationWindow::SimulationWindow()
    : m_simulation(nullptr)
{
    QSurfaceFormat format;
    format.setProfile(QSurfaceFormat::CoreProfile);
    format.setVersion(3, 3);
    format.setSwapInterval(0);
    setFormat(format);

    resize(800, 600);

    connect(this, SIGNAL(frameSwapped()), this, SLOT(requestUpdate()));

//    for (size_t i = 1; i <= 10; i++) {
//        m_agentCounts.push_back(2000 * i);
//    }

    m_agentCounts.push_back(8000);

    m_agentCount = m_agentCounts.begin();
    resetWorkgroupSizes();
}

SimulationWindow::~SimulationWindow()
{
    delete m_agentRenderer;
    delete m_waypointRenderer;
    delete m_obstacleRenderer;
}


void SimulationWindow::initializeGL()
{
    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK) {
        std::cerr << "Unable to initialise GLEW." << std::endl;
        exit(1);
    }

    m_agentRenderer = new AgentRenderer;
    m_waypointRenderer = new WaypointRenderer;
    m_obstacleRenderer = new ObstacleRenderer;
}

void SimulationWindow::resizeGL(int width, int height)
{
    glViewport(0, 0, width, height);

    float viewportRatio = (float)width / height;
    float sceneRatio = SCENE_WIDTH / SCENE_HEIGHT;
    float ratio = viewportRatio / sceneRatio;

    float left = -0.5f * SCENE_WIDTH;
    float right = 0.5f * SCENE_WIDTH;
    float bottom = -0.5f * SCENE_HEIGHT;
    float top = 0.5f * SCENE_HEIGHT;

    if (viewportRatio > sceneRatio) {
        left *= ratio;
        right *= ratio;
    }
    else if (viewportRatio < sceneRatio) {
        bottom /= ratio;
        top /= ratio;
    }

    glm::mat4 matrix = glm::ortho(left, right, bottom, top);

    m_agentRenderer->setMVPMatrix(matrix);
    m_waypointRenderer->setMVPMatrix(matrix);
    m_obstacleRenderer->setMVPMatrix(matrix);
}

void SimulationWindow::paintGL()
{
    static int count = 0;
    static float sum= 0.0f;
//    static float optimalFramerate = 0.0f;
//    static size_t optimalWorkgroupSize = 0;

    float deltaTime = 0.0f;

    if (!m_simulation) {
        m_simulation = new Simulation(*m_agentCount, *m_workgroupSize);

        m_simulation->registerAgentRenderer(m_agentRenderer);
        m_simulation->registerWaypointRenderer(m_waypointRenderer);
        m_simulation->registerObstacleRenderer(m_obstacleRenderer);

        m_simulation->initialize();

        m_time.start();
    }
    else {
        deltaTime = m_time.restart() / 1e3f;

        count++;
        sum += deltaTime;

        if (sum >= REPORT_DELAY) {
            float framerate = count / REPORT_DELAY;

            std::cout << *m_agentCount << " agents, workgroup size = " << *m_workgroupSize << ", " << framerate << " FPS" << std::endl;

            count = 0;
            sum = 0.0f;

//            if (framerate > optimalFramerate) {
//                optimalFramerate = framerate;
//                optimalWorkgroupSize = *m_workgroupSize;
//            }

//            delete m_simulation;
//            m_simulation = nullptr;

//            m_workgroupSize++;

//            if (m_workgroupSize == m_workgroupSizes.end()) {
//                std::cout << *m_agentCount << " agents, workgroup size = " << optimalWorkgroupSize << ", " << optimalFramerate << " FPS" << std::endl;

//                optimalFramerate = 0.0f;
//                m_agentCount++;

//                if (m_agentCount == m_agentCounts.end()) {
//                    exit(0);
//                }
//                else {
//                    resetWorkgroupSizes();
//                }
//            }
        }
    }

    if (m_simulation) {
        m_simulation->animate(deltaTime);

        glClear(GL_COLOR_BUFFER_BIT);
        m_obstacleRenderer->render();
        m_waypointRenderer->render();
        m_agentRenderer->render();
    }
}


void SimulationWindow::resetWorkgroupSizes()
{
    m_workgroupSizes.clear();

//    for (size_t i = 1; i <= 1024; i++) {
//        float num = (float)*m_agentCount / i;

//        if (num == (int)num) m_workgroupSizes.push_back(i);
//    }

    m_workgroupSizes.push_back(125);

    m_workgroupSize = m_workgroupSizes.begin();
}
