#version 330

layout (location = 0) in vec4 aIn;
layout (location = 1) in vec4 bIn;

out vec4 a;
out vec4 b;

void main()
{
    a = aIn;
    b = bIn;
}
