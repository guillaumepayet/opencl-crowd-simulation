#pragma once

#include "AbstractRenderer.h"

#include "ShaderProgram.h"


class AgentRenderer : public AbstractRenderer
{
public:
    AgentRenderer();
    ~AgentRenderer();

    void setList(const IList* list);
    void setMVPMatrix(const glm::mat4& matrix);

    void render();

private:
    ShaderProgram m_program;
    GLuint m_mvpMatrixLoc;
    GLuint m_vao;

    const IList* m_list;
};
