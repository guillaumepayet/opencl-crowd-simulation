#version 330

layout (points) in;
layout (line_strip, max_vertices = 2) out;

in vec4 a[1];
in vec4 b[1];

uniform mat4 mvpMatrix;

void main()
{
    gl_Position = mvpMatrix * a[0];
    EmitVertex();

    gl_Position = mvpMatrix * b[0];
    EmitVertex();

    EndPrimitive();
}
