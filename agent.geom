#version 330

layout (points) in;
layout (triangle_strip, max_vertices = 3) out;

in vec2 velocity[1];
in float radius[1];
in int isActive[1];

uniform mat4 mvpMatrix;

void main()
{
    if (isActive[0] == 0) return;

    vec2 v = velocity[0];
    float angle = -atan(v.y, v.x);

    if (v == vec2(0)) angle = 0;

    mat2 rotation;
    rotation[0] = vec2(cos(angle), -sin(angle));
    rotation[1] = vec2(sin(angle), cos(angle));

    vec4 p = gl_in[0].gl_Position;
    float r = radius[0];

    gl_Position = mvpMatrix * (p + vec4(rotation * vec2(1, 0) * r, 0, 0));
    EmitVertex();

    gl_Position = mvpMatrix * (p + vec4(rotation * vec2(-0.809, 0.588) * r, 0, 0));
    EmitVertex();

    gl_Position = mvpMatrix * (p + vec4(rotation * vec2(-0.809, -0.588) * r, 0, 0));
    EmitVertex();

    EndPrimitive();
}
