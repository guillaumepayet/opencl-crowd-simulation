#pragma once

#include "Shader.h"

#include <glm/glm.hpp>


class ShaderProgram
{
public:
    ShaderProgram();
    ~ShaderProgram();

    void attachShader(const Shader& shader);
    void link();
    GLint getUniformLocation(const char* name);
    void bind();
    void setUniformMatrix(GLint location, const glm::mat4& matrix);

private:
    GLuint m_program;
};
