typedef struct {
    float2 a, b;
} obstacle_t;


float2 obstacle_get_closest_point(__constant obstacle_t* obstacle, float2 position) {
    float2 relative_end_point = obstacle->b - obstacle->a;
    float2 relative_point = position - obstacle->a;

    float lambda = dot(relative_point, relative_end_point) / dot(relative_end_point, relative_end_point);

    if (lambda <= 0.0f) {
        return obstacle->a;
    }
    else if (lambda >= 1.0f) {
        return obstacle->b;
    }
    else {
        return obstacle->a + lambda * relative_end_point;
    }
}
