#pragma once

#include "IList.h"

#include <CL/cl.hpp>

#include <vector>


class ObstacleList : public IList
{
    friend class AgentList;

public:
    static const void* getAOffset();
    static const void* getBOffset();

    static int getStride() { return sizeof(obstacle_t); }

    void add(cl_float2 a, cl_float2 b);

    int getSize() const { return (int)m_list.size(); }
    const void* getBuffer() const { return m_list.data(); }
    int getBufferSize() const { return (int)(m_list.size() * sizeof(obstacle_t)); }

private:
    struct obstacle_t {
        cl_float2 a, b;
    };

    std::vector<obstacle_t> m_list;
};
