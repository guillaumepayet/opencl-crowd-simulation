#pragma once

#include "IList.h"

#include <CL/cl.hpp>

#include <vector>


class WaypointList : public IList
{
    friend class AgentList;

public:
    static const void* getPositionOffset();
    static const void* getRadiusOffset();
    static const void* getIsFinalOffset();

    static int getStride() { return sizeof(waypoint_t); }

    void add(cl_float2 position, cl_float radius, cl_bool isFinal = CL_FALSE);

    int getSize() const { return (int)m_list.size(); }
    const void* getBuffer() const { return m_list.data(); }
    int getBufferSize() const { return (int)(m_list.size() * sizeof(waypoint_t)); }

private:
    struct waypoint_t {
        cl_float2 position;
        cl_float radius;
        cl_bool isFinal;
    };

    std::vector<waypoint_t> m_list;
};
