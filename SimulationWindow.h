#pragma once

#undef __GLEW_H__
#include <QOpenGLWindow>
#define __GLEW_H__

#include <QTime>

#include "Simulation.h"


class SimulationWindow : public QOpenGLWindow
{
public:
    SimulationWindow();
    ~SimulationWindow();

protected:
    void initializeGL();
    void resizeGL(int width, int height);
    void paintGL();

private:
    Simulation* m_simulation;

    AbstractRenderer* m_agentRenderer;
    AbstractRenderer* m_waypointRenderer;
    AbstractRenderer* m_obstacleRenderer;

    QTime m_time;

    std::vector<size_t> m_agentCounts;
    std::vector<size_t>::iterator m_agentCount;
    std::vector<size_t> m_workgroupSizes;
    std::vector<size_t>::iterator m_workgroupSize;
    void resetWorkgroupSizes();
};
