#version 330

layout (location = 0) in vec4 positionIn;
layout (location = 1) in float radiusIn;
layout (location = 2) in int isFinalIn;

out float radius;
out int isFinal;

void main()
{
    gl_Position = positionIn;
    radius = radiusIn;
    isFinal = isFinalIn;
}
