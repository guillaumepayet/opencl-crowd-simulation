#pragma once


class IList
{
public:
    virtual ~IList() {}

    virtual int getSize() const = 0;
    virtual const void* getBuffer() const = 0;
    virtual int getBufferSize() const = 0;
};
