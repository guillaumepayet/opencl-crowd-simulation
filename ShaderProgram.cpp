#include <GL/glew.h>

#include "ShaderProgram.h"

#include <glm/gtc/type_ptr.hpp>

#include <iostream>
#include <fstream>


ShaderProgram::ShaderProgram() : m_program(glCreateProgram()) {}

ShaderProgram::~ShaderProgram()
{
    glDeleteProgram(m_program);
}


void ShaderProgram::attachShader(const Shader& shader)
{
    glAttachShader(m_program, shader.getHandle());
}

void ShaderProgram::link()
{
    glLinkProgram(m_program);

    GLint status;
    glGetProgramiv(m_program, GL_LINK_STATUS, &status);

    if (status == GL_FALSE) {
        GLsizei infoLogLength;
        glGetProgramiv(m_program, GL_INFO_LOG_LENGTH, &infoLogLength);

        GLchar* infoLog = new GLchar[infoLogLength + 1];
        glGetProgramInfoLog(m_program, infoLogLength, nullptr, infoLog);

        std::cerr << "Could not link shader program: " << infoLog << std::endl;

        delete[] infoLog;
    }
}

GLint ShaderProgram::getUniformLocation(const char* name)
{
    return glGetUniformLocation(m_program, name);
}

void ShaderProgram::bind()
{
    glUseProgram(m_program);
}

void ShaderProgram::setUniformMatrix(GLint location, const glm::mat4& matrix)
{
    glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
}
