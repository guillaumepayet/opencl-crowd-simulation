#include "utils.cl"


typedef struct {
    float2 position;
    float radius;
    bool isFinal;
} waypoint_t;


float2 waypoint_get_direction(__constant waypoint_t* waypoint, float2 origin, bool* reached)
{
    float2 diff = waypoint->position - origin;
    float distance = LENGTH(diff);

    if (distance < waypoint->radius) {
        *reached = true;
    }

    return diff / distance;
}
