#include "AgentList.h"

#include <random>


#define SQUARED(x)  (x * x)

#define SQUARED_LENGTH(v)   (SQUARED(v.x) + SQUARED(v.y))

#define LENGTH(v)   sqrtf(SQUARED_LENGTH(v))

#define OBSTACLE_FORCE_SIGMA    0.8f

#define NEIGHBORHOOD_RADIUS 20.0f
#define SOCIAL_RADIUS       2.0f

#define PI  3.1415927f

#define LAMBDA_IMPORTANCE   2.0f
#define GAMMA               0.35f
#define N                   2.0f
#define N_PRIME             3.0f

#define DESIRED_FORCE_FACTOR    1.0f
#define OBSTACLE_FORCE_FACTOR   10.0f
#define LOOK_AHEAD_FORCE_FACTOR 1.0f
#define SOCIAL_FORCE_FACTOR     2.1f


static inline cl_float2 float2(float x, float y)
{
   cl_float2 v;
   v.x = x;
   v.y = y;
   return v;
}

static inline cl_float2 operator * (float f, const cl_float2& v)
{
    return float2(f * v.x, f * v.y);
}

static inline cl_float2 operator + (const cl_float2& a, const cl_float2& b)
{
    return float2(a.x + b.x, a.y + b.y);
}

static inline cl_float2 operator - (const cl_float2& a, const cl_float2& b)
{
    return float2(a.x - b.x, a.y - b.y);
}

static inline cl_float2 operator / (const cl_float2& v, float f)
{
    return float2(v.x / f, v.y / f);
}

static inline float dot(const cl_float2& a, const cl_float2& b)
{
    return a.x * b.x + a.y * b.y;
}

static inline cl_float2& operator += (cl_float2& a, const cl_float2& b)
{
    a = a + b;
    return a;
}

static inline cl_float2& operator *= (cl_float2& v, float f)
{
    v = f * v;
    return v;
}


void AgentList::add(cl_float2 position)
{
    static std::normal_distribution<cl_float> distribution(1.2f, 0.2f);
    static std::default_random_engine generator;

    agent_t agent;
    agent.position = position;
    agent.max_speed = distribution(generator);
    m_list.push_back(agent);
}

void AgentList::compute(const WaypointList& waypoints, const ObstacleList& obstacles)
{
    for (agent_t& agentRef : m_list) {
        agent_t* agent = &agentRef;
        if (!agent->isActive) continue;


        const WaypointList::waypoint_t* waypoint = &waypoints.m_list[agent->waypoint];
        bool reached = false;

        cl_float2 waypoint_direction = waypoint_get_direction(waypoint, agent->position, &reached);

        if (reached) {
            if (waypoint->isFinal) {
                agent->isActive = false;
            }
            else {
                agent->waypoint = (agent->waypoint + 1) % waypoints.m_list.size();
            }
        }

        cl_float2 desired_force = agent->max_speed * waypoint_direction;
        cl_float2 obstacle_force = get_obstacle_force(agent, obstacles.m_list);

        cl_float2 look_ahead_force = get_look_ahead_force(agent, waypoint_direction);
        cl_float2 social_force = get_social_force(agent);

        agent->acceleration = float2(0.0f, 0.0f)
                + DESIRED_FORCE_FACTOR * desired_force
                + OBSTACLE_FORCE_FACTOR * obstacle_force
                + LOOK_AHEAD_FORCE_FACTOR * look_ahead_force    // from 201 to 166 FPS for 5000 agents
                + SOCIAL_FORCE_FACTOR * social_force            // from 201 to 201 FPS for 5000 agents
                + float2(0.0f, 0.0f);                               // from 201 to 111 FPS for 5000 agents
    }
}

void AgentList::move(float deltaTime)
{
    for (agent_t& agent : m_list) {
        if (agent.isActive) {
            agent.velocity += deltaTime * agent.acceleration;

            float speed = LENGTH(agent.velocity);

            if (speed > agent.max_speed) {
                agent.velocity *= (agent.max_speed / speed);
            }

            agent.position += deltaTime * agent.velocity;
        }
    }
}


const void* AgentList::getPositionOffset()
{
    return (const void*)offsetof(agent_t, position);
}

const void* AgentList::getVelocityOffset()
{
    return (const void*)offsetof(agent_t, velocity);
}

const void* AgentList::getRadiusOffset()
{
    return (const void*)offsetof(agent_t, radius);
}

const void* AgentList::getIsActiveOffset()
{
    return (const void*)offsetof(agent_t, isActive);
}



cl_float2 AgentList::waypoint_get_direction(const WaypointList::waypoint_t* waypoint, const cl_float2& origin, bool* reached)
{
    cl_float2 diff = waypoint->position - origin;
    float distance = LENGTH(diff);

    if (distance < waypoint->radius) {
        *reached = true;
    }

    return diff / distance;
}

cl_float2 AgentList::obstacle_get_closest_point(const ObstacleList::obstacle_t* obstacle, const cl_float2& position) {
    cl_float2 relative_end_point = obstacle->b - obstacle->a;
    cl_float2 relative_point = position - obstacle->a;

    float lambda = dot(relative_point, relative_end_point) / dot(relative_end_point, relative_end_point);

    if (lambda <= 0.0f) {
        return obstacle->a;
    }
    else if (lambda >= 1.0f) {
        return obstacle->b;
    }
    else {
        return obstacle->a + lambda * relative_end_point;
    }
}

cl_float2 AgentList::get_obstacle_force(const agent_t* agent, const std::vector<ObstacleList::obstacle_t>& obstacles)
{
    cl_float2 min_diff;
    float min_distance = 1e9f;

    for (size_t i = 0; i < obstacles.size(); i++) {
        const ObstacleList::obstacle_t* obstacle = &obstacles[i];

        cl_float2 closest_point = obstacle_get_closest_point(obstacle, agent->position);
        cl_float2 diff = agent->position - closest_point;
        float distance = LENGTH(diff);

        if (distance < min_distance) {
            min_distance = distance;
            min_diff = diff;
        }
    }

    float distance = min_distance - agent->radius;
    float force_amount = exp(-distance / OBSTACLE_FORCE_SIGMA);
    return force_amount * min_diff / min_distance;
}

cl_float2 AgentList::get_look_ahead_force(const agent_t* agent, const cl_float2& direction)
{
    int count = 0;

    for (size_t i = 0; i < m_list.size(); i++) {
        const agent_t* other = &m_list[i];

        if (!other->isActive || other == agent) continue;


        cl_float2 diff = other->position - agent->position;

        if (SQUARED_LENGTH(diff) > SQUARED(NEIGHBORHOOD_RADIUS)) continue;


        float direction_angle = atan2f(direction.y, direction.x);
        float vv = direction_angle - atan2f(other->velocity.y, other->velocity.x);

        if (vv >  PI) vv -= 2.0f * PI;
        if (vv < -PI) vv += 2.0f * PI;

        if (fabs(vv) > 2.5f) {
            float s = atan2f(diff.y, diff.x) - direction_angle;

            if (s >  PI) s -= 2.0f * PI;
            if (s < -PI) s += 2.0f * PI;

            if ((-0.3f < s) && (s < 0.0f)) {
                count--;
            }
            else if ((0.0f < s) && (s < 0.3f)) {
                count++;
            }
        }
    }

    cl_float2 force;

    if (count < 0) {
        force.x =  0.5f * direction.y;
        force.y = -0.5f * direction.x;
    }
    else {
        force.x = -0.5f * direction.y;
        force.y =  0.5f * direction.x;
    }

    return force;
}

cl_float2 AgentList::get_social_force(const agent_t* agent)
{
    cl_float2 force = float2(0.0f, 0.0f);

    for (size_t i = 0; i < m_list.size(); i++) {
        const agent_t* other = &m_list[i];

        if (!other->isActive || other == agent) continue;


        cl_float2 diff = other->position - agent->position;
        float squared_distance = SQUARED_LENGTH(diff);

        if (squared_distance > SQUARED(SOCIAL_RADIUS)) continue;


        float distance = sqrtf(squared_distance);
        cl_float2 diff_direction = diff / distance;

        cl_float2 diff_velocity = agent->velocity - other->velocity;

        cl_float2 interaction = LAMBDA_IMPORTANCE * diff_velocity + diff_direction;
        float interaction_length = LENGTH(interaction);
        cl_float2 interaction_direction = interaction / interaction_length;

        float theta = atan2f(diff_direction.y, diff_direction.x) - atan2f(interaction_direction.y, interaction_direction.x);

        if (theta >  PI) theta -= 2.0f * PI;
        if (theta < -PI) theta += 2.0f * PI;

        float theta_sign = (theta < 0.0f) ? -1.0f : 1.0f;

        float B = GAMMA * interaction_length;
        float force_velocity_amount = -expf(-distance / B - SQUARED(N_PRIME * B * theta));
        float force_angle_amount = -theta_sign * expf(-distance / B - SQUARED(N * B * theta));

        cl_float2 force_velocity = force_velocity_amount * interaction_direction;
        cl_float2 force_angle = force_angle_amount * float2(-interaction_direction.y, interaction_direction.x);

        force += force_velocity + force_angle;
    }

    return force;
}
