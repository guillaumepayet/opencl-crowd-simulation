#pragma once


#ifdef _WIN32
    #include <Windows.h>
#endif

#ifdef __APPLE__
#   include <OpenGL/gl.h>
#else
#   include <GL/gl.h>
#endif


class Shader
{
public:
    enum Type {
        Vertex = GL_VERTEX_SHADER,
        Geometry = GL_GEOMETRY_SHADER,
        Fragment = GL_FRAGMENT_SHADER
    };

    Shader(Type type, const char* filename);
    ~Shader();

    GLuint getHandle() const { return m_shader; }

private:
    GLuint m_shader;
};
