typedef struct {
    float radius;
    float max_speed;

    float2 position;
    float2 velocity;
    float2 acceleration;

    int waypoint;
    bool isActive;
} agent_t;
