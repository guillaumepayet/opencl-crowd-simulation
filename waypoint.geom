#version 330

#define TWO_PI  6.2831853

layout (points) in;
layout (line_strip, max_vertices = 37) out;

in float radius[1];
in int isFinal[1];

uniform mat4 mvpMatrix;

void main()
{
    if (isFinal[0] == 0) return;

    vec4 p = gl_in[0].gl_Position;
    float r = radius[0];

    float step = TWO_PI / 36.0;

    for (float theta = 0.0; theta <= TWO_PI; theta += step) {
        gl_Position = mvpMatrix * (p + vec4(r * cos(theta), r * sin(theta), 0.0, 0.0));
        EmitVertex();
    }

    gl_Position = mvpMatrix * (p + vec4(r, 0.0, 0.0, 0.0));
    EmitVertex();

    EndPrimitive();
}
