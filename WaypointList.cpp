#include "WaypointList.h"


void WaypointList::add(cl_float2 position, cl_float radius, cl_bool isFinal)
{
    m_list.push_back({ position, radius, isFinal });
}


const void* WaypointList::getPositionOffset()
{
    return (const void*)offsetof(waypoint_t, position);
}

const void* WaypointList::getRadiusOffset()
{
    return (const void*)offsetof(waypoint_t, radius);
}

const void* WaypointList::getIsFinalOffset()
{
    return (const void*)offsetof(waypoint_t, isFinal);
}
